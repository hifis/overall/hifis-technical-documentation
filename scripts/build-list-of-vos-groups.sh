#!/bin/bash

fn_json="./docs/helmholtz-aai/vo-data/vo_groups.json"
fn_mkdn="./docs/helmholtz-aai/list-of-vos-groups.md"


# convert json to markdown table, add it to the prepared header
# remove null values, i.e. replace by empty entries

cat "$fn_json" |
  jq -r '.[] |
  "|\(.name)|`\(.group_claim)`|\(.registered)|\(.last_changed)|\(.contact)|<\(.application_link)>|"' |
  sed -r 's/\|<null>\|/\|\|/g' |
  sed -r 's/\|null\|/\|\|/g' >> "$fn_mkdn"
