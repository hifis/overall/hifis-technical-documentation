---
hide:
  - toc
---

# Documentation for Helmholtz ID, Helmholtz Cloud and Software Development Services

Here you can find public documentation for the services provided via [HIFIS (Helmholtz Federated IT Services)](https://hifis.net).

<div class="grid cards" markdown>

-   __Using Services__

    ---

    Read how you can sign in to our services and collaborate seamlessly worldwide.

    [:octicons-arrow-right-24: Getting started with Helmholtz Cloud Services](helmholtz-cloud/users/getting-started/)
    
    [:octicons-arrow-right-24: Share resources with your collaborators efficiently](helmholtz-cloud/group_managers/getting-started/)

-   __Connecting Services__

    ---

    Read how you can bring your service to people at Helmholtz and partners.

    [:octicons-arrow-right-24: Integration of Helmholtz ID](helmholtz-aai/howto-services.md)

    [:octicons-arrow-right-24: Service Onboarding](helmholtz-cloud/providers/getting-started.md)

</div>

## Feedback and Support
Please [**contact us**](https://hifis.net/contact.html) if you are missing any content or if you have general comments.

You are also invited to [contribute to this documentation via the corresponding repository](https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation) (public readable, but AAI login required for write access).