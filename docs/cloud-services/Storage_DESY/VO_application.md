# Apply for usage

Usage of the DESY-based dCache InfiniteSpace service is not possible (and not reasonable) for single users.

As a responsible manager of a group, managed as [Virtual Organisation](../../helmholtz-aai/howto-vos.md) (VO or sub-VO), you can apply for usage as stated below.

!!! info "Why apply?"
    **This service requires some expert knowledge within your group / VO.**

    - This service is intended for large scale data handling: It provides a web front-end for _very_ basic operations, however all reasonable usage scenarios require some knowledge on linux command-line operations.
    - HIFIS can provide **limited support** in setting up your storage environment -- however for sustainable operation, **you need to have people with such basic knowledge in your group**.
    - Depending on your use case, we might advise you to refer to more convenient storage solutions, like [Sync&Share](https://helmholtz.cloud/services/?filterKeywordIDs=860a8f9a-54aa-4868-9747-5df90d0e35e0).
    - The dCache InfiniteSpace scales horizontally avoiding the bottleneck of a single point of entry. Clients accessing data are always redirected directly to a data server. This comes with the caveat that you and your partner sites have to allow incoming and outgoing connections on higher ports to different nodes. The regular HTTPS port is only used for the browser frontend, all other operations happen with different nodes on different ports. This might cause problems with some of your partner sites depending on their firewall configurations.

## Application procedure

### 1. Log in once to the central DESY infrastructure proxy (Keycloak)

Log in once so that your log in data is known and can be mapped correctly in subsequent steps:

- Log in at [DESY keycloak](https://keycloak.desy.de/auth/realms/production/account/#/), by clicking on "Helmholtz AAI" and selecting your institution.

### 2. Send a mail to [support@hifis.net](mailto:support@hifis.net?subject=[hifis-storage])

Send a mail in English or German, stating some basic infos that are needed to set things up:

#### You are already manager of a VO?

- Please provide the exact name of your VO (and optionally Sub-VOs) that you want to give access to storage data.

#### You need a new VO?

- Please provide a brief purpose of your project (1-2 Sentences)
- Who are the responsible contact person(s) - at least 1, better 2
- (Short) Name of your Group, as it will appear in folder structures and [VO naming](../../helmholtz-aai/list-of-vos.md) schemes
- Acceptable use policy, [template can be found here](../../helmholtz-aai/policies/helmholtz-aai/04_VO-AUP-Template.odt)

#### Further we need:

- Acknowledgement of the [Acceptable Use Policy](AUP.md) of the Service

#### And we are happy if you provide us further with...

- Some usage details that allow us to assist you in setting up the best possible solution:
    - Users from which Helmholtz Centres or other institutions shall have full access to the data
    - Approximate amount of storage needed
    - Approximate time frame of storage needed
    - Any specific requirements on backup, availability, data transfer, use cases
- Preferred [Access Management Model](access_models.md):
    - [Simple](access_models.md#simple-model) Model (_default_), or
    - [Self-managed Access](access_models.md#self-managed-access-model) Model

### 3. We will come back to you with further instructions

Stand by. We will contact you again and try to find the optimal solution for you.
