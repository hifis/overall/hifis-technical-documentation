---
hide:
  - toc
---

# Helmholtz Cloud

<div class="grid cards" markdown>

-   __Start Using Services__

    ---

    Get started using Helmholtz Cloud resources in just a few minutes.

    [:octicons-arrow-right-24: Get started now](../helmholtz-cloud/users/getting-started/)

-   __Providing a Service__

    ---

    Read how you can bring your service to people at Helmholtz and partners.

    [:octicons-arrow-right-24: Become part of Helmholtz Cloud](../helmholtz-cloud/providers/getting-started/)


</div>


## About Helmholtz Cloud

Helmholtz Cloud supports scientists at every stage of the research process, from proposal to publication. Scientists can easily access AI acceleration hardware, collaborative LaTeX editing, project management tools or state-of-the-art communication tools among many other high-quality services. The main point of access is [**helmholtz.cloud**](https://helmholtz.cloud).

Helmholtz Cloud is the federated cloud platform of Germany’s largest scientific organization – the Helmholtz Association. Here, Helmholtz centers share digital resources and make them **available to Helmholtz members and international partners**. The Helmholtz-wide platform HIFIS has the mission to develop and operate the cloud platform.

## More Services
If you are curious about [upcoming services](https://plony.helmholtz.cloud/sc/public-service-portfolio),you can view the full list of services over at our Service Portfolio Management Tool Plony (no login necessary). 

Go here to find a full list of [services which are connected to Helmholtz ID](https://login.helmholtz.de/unitygw/hifis/files/connected-services.html), but are not necessarily covered by the management and governance of the Helmholtz Cloud
