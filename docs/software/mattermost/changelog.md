---
title: Service Changelog
---

## 2025-01-10 - Mattermost 10.2

Update Mattermost to version 10.2 - [Changelog](https://docs.mattermost.com/about/mattermost-v10-changelog.html#release-v10-2-feature-release)

* A warning is now shown when deleting a post or comment from a remote/shared channel.
* Bot messages will now properly mention both users when they happen on non-bot Direct Messages.
* Updated the channel header to hide pinned posts when there aren't any in the channel.
* Added full support for @mentions in the values of fields in message attachments.

## 2024-12-12 - Mattermost 10.1

Update Mattermost to version 10.1 - [Changelog](https://docs.mattermost.com/about/mattermost-v10-changelog.html#release-v10-1-feature-release)

* Enabled Channel Bookmarks, added re-ordering, and fixed URL validity checking.
* Added channel specific message notification sounds configuration.

## 2024-11-04 - Mattermost 10.0

Update Mattermost to version 10.0 - [Changelog](https://docs.mattermost.com/about/mattermost-v10-changelog.html#release-v10-0-major-release)

* Added Do not disturb and late timezone warnings to Direct Message posts.
* Added labels for channel header and purpose in the right-hand side channel info view.
* Made various improvements to code involving user preferences.

## 2024-10-11 - Mattermost 9.11

Update Mattermost to version 9.11 - [Changelog](https://docs.mattermost.com/about/mattermost-v9-changelog.html#release-v9-11-extended-support-release)

* Added a message “Editing this message with an @mention will not notify the recipient” in the post edit dialog.
* Made the appearance of several tooltips more consistent.
* Updated the help text in the Direct Messages modal.
* Emojis are now placed at cursor position while editing messages.

## 2024-08-19 - Mattermost 9.10

Update Mattermost to version 9.10 - [Changelog](https://docs.mattermost.com/about/mattermost-v9-changelog.html#release-v9-10-feature-release)

* Re-designed the user profile popover and improved its performance.
* Added banner to prompt users to give desktop notification permissions when opening the app.

## 2024-07-25 - Mattermost 9.9

Update Mattermost to version 9.9 - [Changelog](https://docs.mattermost.com/about/mattermost-v9-changelog.html#release-v9-9-feature-release)

* Added UI improvements to the core layout.
* Updated the channel header layout to reduce height and simplify the UI.

## 2024-07-01 - Mattermost 9.8

Update Mattermost to version 9.8 - [Changelog](https://docs.mattermost.com/deploy/mattermost-changelog.html#release-v9-8-feature-release)

* Combined Desktop and Mobile notifications in the user settings modal.
* Added a Don't Clear option for Do Not Disturb.
* Enhanced the user interface for channel introductions.
* Added emoji tooltips on hover in post message.
* Made the appearance of several tooltips more consistent.
* Updated the right-hand side Thread view to use relative timestamps to be more consistent with the global Threads view.
* Added a total reply count to the right-hand side thread view.

## 2024-06-27 - Mattermost 9.7

Update Mattermost to version 9.7 - [Changelog](https://docs.mattermost.com/deploy/mattermost-changelog.html#release-v9-7-feature-release)

* Redesign of team settings and user settings modals in the user interface.
* The first emoji is now auto-selected in the emoji picker.
* Added Markdown support for batched email notifications. 
* Users' timezone is now used in batched email notifications.

## 2024-05-27 - Mattermost 9.6

Update Mattermost to version 9.6 - [Changelog](https://docs.mattermost.com/deploy/mattermost-changelog.html#release-v9-6-feature-release)

* Redesign of profile/account menus in the user interface.
* Added support for WebP image previews in the web app similar to PNG and other image formats.

## 2024-05-16 - Mattermost 9.5

Update Mattermost to version 9.5 - [Changelog](https://docs.mattermost.com/deploy/mattermost-changelog.html#release-v9-5-extended-support-release)

* Improved the behavior of suggestion boxes when changing the caret position.
* Changed the time for tomorrow in the Do Not Disturb timer and post reminder to refer to the next day at 9:00am instead of 24hrs from the time of activation.
* Updated message timestamp tooltips to include seconds.
* Some performance optimizations.

## 2024-02-19 - Mattermost 9.4

Update Mattermost to version 9.4 - [Changelog](https://docs.mattermost.com/deploy/mattermost-changelog.html#release-v9-4-feature-release)

* Redesign of channel notifications modal in the user interface.
* Emojis are now enlarged in emoji tooltips on mouse hover.
* Adjusted the position of the suggestion list in "Add to a channel" modal to be below or above the text field.

## 2024-01-03 - Mattermost 9.3

Update Mattermost to version 9.3 - [Changelog](https://docs.mattermost.com/deploy/mattermost-changelog.html#release-v9-3-feature-release)

* Updated the Settings modal with an improved user interface.
* Added a new Jump to recents banner when a channel is scrolled up.
* Modified the behavior of the code button (Ctrl+Alt+C) to create inline codes or code blocks.
* Disabled markdown keybindings within code blocks.

## 2024-01-03 - Mattermost 9.2

Update Mattermost to version 9.2 - [Changelog](https://docs.mattermost.com/deploy/mattermost-changelog.html#release-v9-2-feature-release)

* Improved readability by displaying system messages on multiple lines when editing a channel header.
* Combined "joined/left" event types in system messages.

## 2023-11-30 - Mattermost 9.1

Update Mattermost to version 9.1 - [Release Post](https://mattermost.com/blog/mattermost-v9-1-is-now-available/)

* Added the ability to resize the channel sidebar and right-hand sidebar.
* Added two new filtering options (show all channel types and show private channels) to the Browse channels modal.

## 2023-11-07 - Mattermost 9.0

* Update Mattermost to version 9.0 -
  [Release Post](https://mattermost.com/blog/mattermost-v9-0-is-now-available/)
    * The Boards plugin is no longer bundled with Mattermost and will be continued as a
      [community-supported plugin](https://forum.mattermost.com/t/upcoming-product-changes-to-boards-and-various-plugins/16669).

## 2023-10-19 - Mattermost 8.1

* Update Mattermost to version 8.1 -
  [Release Post](https://mattermost.com/blog/mattermost-security-updates-8-1-2-esr-8-0-3-7-8-11-esr-released/)
    * Updated the user interface for the Browse channels modal. 
    * Increased the nickname field in the user interface from 22 to 64 characters.

## 2023-10-19 - Mattermost 8.0

* Update Mattermost to version 8.0 -
  [Release Post](https://mattermost.com/blog/mattermost-security-updates-8-0-1-7-10-5-7-8-9-esr-released/)
    * Improved database performance and efficiency.

## 2023-05-30 - Mattermost 7.10

* Update Mattermost to version 7.10 -
  [Release Post](https://mattermost.com/blog/mattermost-security-updates-7-10-1-7-9-4-7-8-5-esr-released/)
    * Channels: Set a reminder to read a message at a specific time
    * Channels: Mentions from muted channels are no longer shown on the browser tabs
    * Channels: Custom user status is now shown in the right-hand side Members pane
    * Channels: Invite multiple people at a time by email

## 2023-04-25 - Mattermost 7.9

* Update Mattermost to version 7.9 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-9-is-now-available/)
    * Boards: Team Administrator access to boards

## 2023-03-31 - Mattermost 7.8

* Update Mattermost to version 7.8 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-8-is-now-available/)
    * Boards: Improved filters and groups

## 2023-02-24 - Mattermost 7.7

* Update Mattermost to version 7.7 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-7-is-now-available/)
    * Channels: Mobile app v2 general availability
    * Channels: Message priority
    * Boards: File attachments
    * Boards: Sidebar drag and drop
    * Boards: Improved template picker
    * Playbooks: Run playbooks in an existing channel
    * Playbooks: Task Inbox

## 2023-01-03 - Mattermost 7.5

* Update Mattermost to version 7.5 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-5-is-now-available/)
    * Boards: Additional standard board templates
    * Boards: Filter by text properties
    * Channels: User last activity

## 2022-11-24 - Mattermost 7.4

* Update Mattermost to version 7.4 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-4-is-now-available/)
    * Boards: Additional board roles
    * Boards: Minimum default board roles
    * Boards: Add board members via autocomplete list
    * Boards: Channel notifications for linked boards
    * Boards: Multi-person property

## 2022-10-27 - Mattermost 7.3

* Update Mattermost to version 7.3 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-3-is-now-available/)
    * Boards: New role-based permissions system
    * Boards: New sidebar navigation
    * Boards: Link multiple boards to channels
    * Boards: Custom template permissions
    * Playbooks: Redesigned left-hand sidebar and run detail page

## 2022-10-04 - Mattermost 7.2

* Update Mattermost to version 7.2 coming with Message Forwarding -
  [Release Post](https://mattermost.com/blog/mattermost-v7-2-is-now-available/)

## 2022-08-25 - Mattermost 7.1

* Update Mattermost to version 7.1 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-1-is-now-available/)

## 2022-07-27

* Update Mattermost to version 7.0 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-0-is-now-available/)
    * [Collapsed Reply Threads](https://mattermost.com/blog/mattermost-v7-0-is-now-available/#collapsed) enabled by default
    * Completely redesigned [Message Formatting Toolbar](https://mattermost.com/blog/mattermost-v7-0-is-now-available/#toolbar)
    * Multiple [Playbooks improvements](https://mattermost.com/blog/mattermost-v7-0-is-now-available/#inline)

## 2022-07-08

* Update Mattermost to version 6.7 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-7-is-now-available/)
    * Playbooks: [Set task due dates for playbook runs](https://mattermost.com/blog/mattermost-v6-7-is-now-available/#playbooks)

## 2022-06-21

* Update Mattermost to version 6.6 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-6-is-now-available/)
    * Channels: [Triggers and actions](https://mattermost.com/blog/mattermost-v6-6-is-now-available/#triggers)
    * Channels: [New location for message action](https://mattermost.com/blog/mattermost-v6-6-is-now-available/#message)

## 2022-04-26

* Update Mattermost to version 6.5 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-5-is-now-available/)
    * [Cross-team channel navigation](https://mattermost.com/blog/mattermost-v6-5-is-now-available/#cross)
    * [Import, export, and duplicate Playbooks](https://mattermost.com/blog/mattermost-v6-5-is-now-available/#playbooks)
    * [Improved share Boards UI](https://mattermost.com/blog/mattermost-v6-5-is-now-available/#share).
* Update Mattermost to version 6.4 -
[Release Post](https://mattermost.com/blog/mattermost-v6-4-is-now-available/)
    * Restriction of [one Playbook per team](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#playbooks) has been removed.
    * Multiple Boards enhancements including
        * new [standard templates](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#standard), 
        * new [template previews](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#template),
        * new [archive format with image support](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#archive), 
        * new [card badges](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#badges).

## 2022-02-25

* Update Mattermost to version 6.3 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-3-is-now-available/)

## 2022-01-28

* Update Mattermost to version 6.2 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-2-is-now-available/)

## 2022-01-14

* Update Mattermost to version 6.1 -
  [Release Post](https://www.hifis.net/news/2022/01/14/mattermost-6-available)

## 2021-11-30

* Update Mattermost to version 5.39 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-39/)
    * Looking ahead to general availability of 
      [Collapsed Reply Threads](https://mattermost.com/blog/mattermost-v5-39/#collapsed)

## 2021-10-01

* Update Mattermost to version 5.38 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-38/)
    * Enhanced user 
      [onboarding experience](https://mattermost.com/blog/mattermost-v5-38/#onboarding)

## 2021-08-26

* Update Mattermost to version 5.37 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-37/)
    * Try out [Collapsed Reply Threads](https://mattermost.com/blog/collapsed-reply-threads-beta/)
    * Enable yourself in **Account Settings > Display > Collapsed Reply Threads (Beta)**.
