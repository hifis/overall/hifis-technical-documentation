# List of registered Virtual Organisations (VO)
This document lists the Virtual Organisations (VO) registered in the Helmholtz AAI. Currently, VOs can represent working groups or Helmholtz centres.

VO memberships of a logged in user are transmitted by the Helmholtz AAI via the [`group` claim as documented here](attributes.md#group-membership-information).

## VOs of Working groups
Click on the corresponding link if you want to apply for a membership in the respective VO. This will send a corresponding mail to the HIFIS helpdesk.
Depending on the VO policies, you may need to provide a reason on why you would like to become member.

The complete list of Top-Level VOs in the `helmholtz.de` and `nfdi.de` and `h-df.de` [namespaces](namespaces-registry.md) is given here:

- [Table](list-of-vos-groups.md)
- [json](vo-data/vo_groups.json)

The required minimum [Assurance](assurance.md) in all `helmholtz.de` cases is [AARC Assam](https://aarc-community.org/guidelines/aarc-g021/) (approximately corresponding to DFN AAI Basic).

## VOs representing Helmholtz centres

The VO of the centres and Helmholtz-members are managed automatically by authentication via the IdPs of the centres.

The required [Assurance](assurance.md) in all cases is RAF Cappuccino.

| VO Name           |  group claim, prefix:<br>`urn:geant:helmholtz.de:group:`      |  Registered  |  Last Changed  |
| ----------------- | ----------------------------------------------------------------------- | - | -  |
|  AWI    | `AWI`   | 22.07.2020 | 22.07.2020  |
|  CISPA  | `CISPA` | 22.07.2020 | 22.07.2020  |
|  DESY   | `DESY`  | 22.07.2020 | 22.07.2020  |
|  DKFZ   | `DKFZ`  | 22.07.2020 | 22.07.2020  |
|  DLR    | `DLR`   | 22.07.2020 | 22.07.2020  |
|  DZNE   | `DZNE`  | 22.07.2020 | 22.07.2020  |
|  FZJ    | `FZJ`   | 22.07.2020 | 22.07.2020  |
|  GEOMAR | `GEOMAR`| 22.07.2020 | 22.07.2020  |
|  GFZ    | `GFZ`   | 22.07.2020 | 22.07.2020  |
|  GSI    | `GSI`   | 22.07.2020 | 22.07.2020  |
|  hereon | `hereon`| 22.07.2020 | 13.04.2021  |
|  HMGU   | `HMGU`  | 22.07.2020 | 22.07.2020  |
|  HZB    | `HZB`   | 22.07.2020 | 22.07.2020  |
|  HZDR   | `HZDR`  | 22.07.2020 | 22.07.2020  |
|  HZI    | `HZI`   | 22.07.2020 | 22.07.2020  |
|  Helmholtz-GS | `Helmholtz-GS` | 22.07.2020 | 22.07.2020  |
|  KIT    | `KIT`   | 22.07.2020 | 22.07.2020  |
|  MDC    | `MDC`   | 22.07.2020 | 22.07.2020  |
|  UFZ    | `UFZ`   | 22.07.2020 | 22.07.2020  |

Members of any of these VOs are automatically also member of:

| VO Name           |  group claim, prefix:<br>`urn:geant:helmholtz.de:group:`      |  Registered  |  Last Changed  |
| ----------------- | ----------------------------------------------------------------------- | - | -  |
|  Helmholtz-member | `Helmholtz-member` | 22.07.2020 | 22.07.2020  |
