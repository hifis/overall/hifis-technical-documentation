# How to transfer MFA information during user's login

## MFA login

Helmholtz ID uses Multi-factor Authentication (MFA) in two cases:

- It is [mandatory for group/VO management](https://hifis.net/doc/helmholtz-aai/howto-mfa/) since mid 2023.
- It can be activated by users for any other login.

## Simplified MFA-backed login: MFA Honoring

The query to provide MFA in Helmholtz ID is **skipped** if the following conditions are fulfilled:

- The user provided a second factor already during log in at the home IdP
- The home IdP transmits the MFA information as follows:
    - Ensure to fulfil the requirements of the [REFEDS MFA profile](https://refeds.org/profile/mfa)
    - Send `https://refeds.org/profile/mfa` in the `AuthnContextClassRef` element of the SAML Assertion


## Need help?

[**Contact us**](mailto:support@hifis.net?subject=[aai]) if you need help.
