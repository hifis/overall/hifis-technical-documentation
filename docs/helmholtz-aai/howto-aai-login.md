# As a user: How to log in to cloud services

1. Go to a service page, e.g. listed in [Cloud Portal](https://helmholtz.cloud/services)
1. Click on Log in Button.
    - **Never log in directly on the service page.**
    - Instead, search for "Helmholtz ID" / "Helmholtz AAI".
1. Search for your institution, or alternatively ORCID, Google, Github if that suits you more.
1. Log in with your normal username, password etc
1. If it's your first time, accept usage conditions, etc.; accept data transfers.
1. You should end up in the service and see your content.
