---
hide:
  - toc
---

# Local Testing and Development (HIDaH)

<figure markdown="span">
  ![HIDaH Screenshot](../images/id/hidah_screenshot.png){ width="60%" }
  <figcaption>You can run HIDaH locally to test your Helmholtz ID integration.</figcaption>
</figure>

HIDaH (Helmholtz ID at Home) is a lightweight OpenID Connect provider emulating Helmholtz ID for *testing and development*. It implements a basic login flow allowing you to set the user profile attributes forwarded to your application. This way you can test how your application reacts to different attributes (e.g. group memberships) set by Helmholtz ID.
HIDaH is available as a simple container you can run alongside your development setup and provides an alternative to testing against a live environment or having to run a full OIDC/IdP setup on your machine. You can find the current HIDaH release, documentation and source code here:

[Go to HIDaH Project](https://codebase.helmholtz.cloud/hifis/cloud/access-layer/cloud-portal/hidah){ .md-button .md-button--primary } <br /><br />

Please note this is a community-developed tool for development purposes only. Feel free to open issues and contribute on Helmholtz Codebase!

