# How to interact with Unity via `oidc-agent`

`oidc-agent` is a set of tools to manage OpenID Connect tokens and make them easily usable from the command line.

## Installing the agent

For installing the agent follow the instructions on the website of `oidc-agent`:

<https://indigo-dc.gitbook.io/oidc-agent/intro>

A growing number of Linux distributions have `oidc-agent` already in their repository. If you use one of these distributions you may want to use the packaged version.

## Create a Configuration for the Helmholtz AAI

`oidc-agent` comes with a number of pre-configured public client configurations. There is also a public client for the Helmholtz AAI which makes configuration easy.

-   In order to start the configuration process: `oidc-gen --pub <shortname>`
-   Enter `https://login.helmholtz.de/oauth2/` as issuer or select the corresponding number from the presented list.
-   Select the scopes you want to access or accept the preconfigured list.
-   Enter the desired redirection URLs (you have to add at least one - if you only plan to use the CLI agent, this doesn't really matter, so you can use e.g. `http://localhost:8080`)
-   Important: Login with your home IdP in the browser window which opens. This step is necessary to complete the account configuration.
-   as a final step, add a password for the generated configuration. You will need this password everytime, you add the configuration to `oidc-agent` so note down your passphrase carefully
- start `oidc-agent` and set the necessary environment variables with ``eval `oidc-agent` ``
- check if your configuration works with `oidc-token <short name of your configuration>`

!!!warning "Nota bene"
    - If you are configuring the `oidc-agent` on a computer without an XSession (e.g. on a machine where you are logged in with pure `ssh`, you will need to use the command `oidc-gen --pub --no-url-call --no-webserver <shortname>`. The agent will then print out a URL you can copy into a browser of your choice, log in as usual and you will get a message "Page not found". Copy the URL in the URL-bar of the browser and follow the instructions given by `oidc-agent` in your `ssh`-session before.
    - If you are logged into a machine via `ssh` and the `$DISPLAY` environment variable is not set, `oidc-agent` will assume that you are on a "dumb" device and invoke an authorization flow that is deactivated in the Helmholtz AAI. Your configuration attempt will fail unless you set `export DISPLAY=0:` assuming you are on a Linux machine.

## Accessing user information

After configuring `oidc-agent` and logging in you can now start using it. You can, for example, request your users information with `curl`:

``curl -H "Authorization: Bearer `oidc-token <short name of your configuration>`" -X GET https://login.helmholtz.de/oauth2/userinfo``

If you want to have a nice, readable output, you can format the result with `jq` (you will probably have to install jq before that):

``curl -H "Authorization: Bearer `oidc-token <short name of your configuration>`" -X GET https://login.helmholtz.de/oauth2/userinfo | jq .``

This should give you something like:

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1227  100  1227    0     0   7967      0 --:--:-- --:--:-- --:--:--  7967
{
  "sub": "06b05656-ad50-11ed-ac2b-3f78a68a37a7",
  "email_verified": true,
  "groups": [
    "/Helmholtz-member",
    "/HDF",
    "/"
  ],
  "preferred_username": "sam.scientist",
  "display_name": "Sam Scientist",
  "given_name": "Sam",
  "eduperson_principal_name": "sam.scientist@helmholtz.de",
  "eduperson_entitlement": [
    "urn:geant:h-df.de:group:HDF#login.helmholtz.de",
    "urn:geant:h-df.de:group:Helmholtz-member#login.helmholtz.de"
  ],
  "name": "Sam Scientist",
  "eduperson_unique_id": "06b05656ad5011edac2b3f78a68a37a7@login.helmholtz.de",
  "sn": "Scientist",
  "eduperson_assurance": [
    "https://refeds.org/assurance/ATP/ePA-1m",
    "https://refeds.org/assurance/ID/unique",
    "https://refeds.org/assurance/IAP/low"
  ],
  "family_name": "Scientist",
  "email": "sam.scientist@helmholtz.de"
}
```
