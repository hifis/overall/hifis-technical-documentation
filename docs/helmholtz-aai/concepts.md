# Concepts

The Helmholtz ID is organised in [Virtual Organisations (VOs)](concepts.md#the-virtual-organisation-vo-concept),
and based on the [Proxy Concept](#the-sp-identity-provider-proxy-concept) introduced by the [AARC
Blueprint](https://aarc-project.eu/architecture).

## The Virtual Organisation (VO) Concept

Virtual Organisations (i.e. research groups) are the key element of authorisation in the Helmholtz Cloud.

The classical Virtual Organisation (VO) concept applies to HPC but also to the grid world:

- Each VO consists of a number of users.

- A VO is supported by an arbitrary number of services, that for example can be used for collaboration, grant access to computing infrastructure and much more.

- VOs are managed by VO administrators (aka Principal Investigators). [Administrators of Virtual Organisations](howto-vos.md) are responsible for services in their VO (e.g. by writing proposals for HPC computing time, or by otherwise obtaining quotas on specific pieces of infrastructure).  

- VO administrators are also responsible for managing the VO's members by means of the [Group Management interface of Unity](https://login.helmholtz.de/upman)

In Helmholtz ID we maintain a [List of VOs](list-of-vos.md) that indicates each VO's contact person and the assurance level required to join.

## The SP-Identity Provider Proxy Concept

The Helmholtz ID is modular in the sense that it involves one or optionally more SP-IdP proxies / software stacks.

According to the [AARC Blueprint](https://aarc-project.eu/wp-content/uploads/2019/04/Blueprint-development-AARC-BPA-2019-v0.2-abingdon-2000px.jpg), 
an SP-IdP proxy is a software stack which can be used for hierarchical organisation of an Authentication and Authorisation Infrastructure (AAI). 
The SP-IdP Proxy addresses three common shortcomings in this field:

1. With growing numbers of SPs and IdPs it becomes very difficult to maintain a consistent attribute release from each IdP to every SP. The Proxy reduces this n:m mapping to a n:1:m mapping.
2. Some authorisation attributes can not be maintained at home IdPs. For example, group membership needs to be managed by the head of a group and not by administrative personnel operating a home IdP.
    - Some authentication patterns, in which access to individual datasets is required, may be supported by specialised SP-IdP-Proxy implementations.
3. Additionally, a proxy, once in place, allows translation between different AAI protocols. At this moment, proxies can enable smooth interoperation between X.509 certificates, SAML and OpenID Connect.

A typical use case would be, for example, if a service is not able to directly address SAML- or OpenID Connect-federated identity providers. In such case, a so-called SP-IdP proxy can mediate the services native protocol on the one end, and handle the complexity of a federation on the other end. 
This is achieved by offering the services suitable SSO access points, and by acting as a service provider to the federation, redirecting authentication towards the central login proxy or in some cases forwarding the users to their home IdPs for authentication.

### Central and distributed software stacks
Advanced configurations allow hierarchical interconnections between multiple proxies (as illustrated in [this article](https://dev.fim4r.org/2021/03/04/getting-started-with-aarc-guidelines/)). 
The top of such a hierarchy is a community AAI, which can support services directly, just as it can support infrastructure proxies, that have additional services connected to it.
For a given community, there is one (central) software stack implementing the SP-IdP-Proxy for the community (which is why it is also called "Community-AAI").

The central software stack in Helmholtz ID (i.e., the Community AAI for Helmholtz) is based on Unity, operated by FZJ, which serves as the primary login authority.

Additional software stacks (services and Infrastructure Proxies) are operated at various sites. 
A relevant example for this is the Reg-App, developed and operated at KIT.
Others are, for example, Keycloak and Keystone software, operated at several sites.

Unity, Reg-App (and in many cases also other SP-IdP Proxies / software stacks) provide first-class support for the OIDC and SAML protocols. 
They can connect SAML Identity Providers, OIDC Providers, SAML Service Providers and OIDC Relying Parties, thus
enabling teams to use their preferred identity sources and services, regardless of the authentication protocol.

![Sketch of AAI concept](images/AAI-concept.svg)

### Specialities of proxies

Unity focusses for example on community management and [virtual organisations (VOs)](concepts.md#the-virtual-organisation-vo-concept), including social providers.
Other proxies can add functionality to this. 
For example, the Reg-App adds support for enhanced security restrictions concerning [two-factor authentication and SSH key management](../core-services/hpc-integration.md).
An LDAP-based infrastructure proxy is also integrated there. 
This Reg-App functionality is available for all services.

The local SP-IdP proxies are responsible of referring to the central proxy (Unity) in order to synchronise user attributes, including mapping of potential local user ids to the unique user id provided by Unity.

### Discovery services
The Proxies are responsible for aggregating the user attributes from various
identity sources, enforcing community and platform wide policies and
providing one persistent user identifier and a harmonised set of
attributes to the connected services. The discovery services provide a
web interface for users to search and select their preferred identity
provider. 

The discovery services are integrated with the proxies and enable them to
operate with all identity providers supported in the same way. For this
they aggregate the metadata of all the SAML Identity and Service Providers
that are connected to the platform. This is done by aggregating the metadata
feed of eduGAIN, while allowing the platform administrators to also configure
other local or remote metadata sources, like social providers. The meta data service is an essential
component of the platform directly connected to the proxies.

## The Identity Assurance Concept

The [REFEDS Assurance Framework (RAF)](https://refeds.org/assurance) defines individual assurance components
that describe a users identity. This allows for differentiation between the
quality of the ID-Vetting, the attribute freshness and the uniqueness of the
identifiers used.

Since today none of the existing IdPs support RAF, the idea is to do this
on the proxy level. I.e. Unity will convey information about the upstream
authentication of the user. This is hard coded configuration that contains
a priori knowledge about the upstream IdP and its procedures. For
example will this allow us to characterise the upstream authentication and
differentiate between ORCID users and those from (for example) KIT.

Currently defined are:

- ID-Uniqueness:
    - `$PREFIX$/ID/unique`
    - `$PREFIX$/ID/no-eppn-reassign`
    - `$PREFIX$/ID/eppn-reassign-1y`

- ID-Proofing:
    - `$PREFIX$/IAP/low`
    - `$PREFIX$/IAP/medium`
    - `$PREFIX$/IAP/high`
    - `$PREFIX$/IAP/local-enterprise`

- Attribute freshness:
    - `$PREFIX$/ATP/ePA-1m`
    - `$PREFIX$/ATP/ePA-1d`

