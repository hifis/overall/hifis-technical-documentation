# How to do the basic steps in Helmholtz ID / AAI

In a decentralized, networked model of cloud services known as federated cloud, the federated Authentication and Authorization Infrastructure (Helmholtz AAI) plays a crucial role. Proper usage of this system requires a few key steps and some foundational knowledge to avoid common pitfalls.

See here how to do the most important steps:

- A) As a user: [**How to log in to cloud services**](howto-aai-login.md)
- B) As a user being invited to a group: [**How to join a group (VO)**](howto-aai-joinvo.md)
- C) As a group leader/PI: [**How to create and manage a group (VO)**](howto-aai-createvo.md)

In all cases: If anything breaks, check [FAQ](https://hifis.net/faq#heaai), or [contact us](mailto:support@hifis.net).
