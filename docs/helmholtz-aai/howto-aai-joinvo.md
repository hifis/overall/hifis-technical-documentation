# As a user being invited to a group: How to join a group (VO)

1. In order to join a group ("virtual organisation", VO), you should have received a mail from Helmholtz ID / AAI with a link. That mail has been [triggered by your group manager](howto-aai-createvo.md).
1. Make sure that you do not have multiple login tabs open (maximum one).  
   Otherwise, multiple parallel logins may cause confusion in the following steps.
1. Click the link.
1. Log in with the desired account (your organisation, or ORCID, Google, Github) as [described here](howto-aai-login.md).
1. Accept VO Policies and enter the group.
1. _Optional:_ You can check your group membership(s) anytime at <https://login.helmholtz.de/home/>. After the previous steps, the desired group should appear here.
1. **To make the new group membership have effect**, e.g. gaining access to group's data and resources, **you need to log in anew to that service**. That is:
    - If you're logged in to the service already, log out.
    - If the service is in DESY (e.g. dCache), also [log out from DESY's Login portal Keycloak](https://keycloak.desy.de/auth/realms/production/protocol/openid-connect/logout).
    - Log in to the service again.
1. If you still miss to see your group share in DESY Sync&Share:
    - Check "pending shares": <https://syncandshare.desy.de/index.php/apps/files/pendingshares>
1. You should see the content belonging to your new group.
