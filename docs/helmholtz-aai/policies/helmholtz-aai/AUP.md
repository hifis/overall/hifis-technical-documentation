# Helmholtz AAI Acceptable Use Policy

## INFRASTRUCTURE AIMS AND PURPOSES

This Infrastructure is operated for the purpose of federated access to services provided within the Helmholtz Federated IT Services (HIFIS) platform. Individual services within the Infrastructure may present additional Acceptable Use Policies.

## USER DECLARATION

By registering as a user you declare that you have read, understood and will abide by the following conditions of use:

1. You shall only use the Services in a manner consistent with the purposes and limitations described above; you shall show consideration towards other users including by not causing harm to the Services; you have an obligation to collaborate in the resolution of issues arising from your use of the Services.
2. You shall only use the Services for lawful purposes and not breach, attempt to breach, nor circumvent administrative or security controls.
3. You shall respect intellectual property and confidentiality agreements.
4. You shall protect your access credentials (e.g. passwords, private keys or multi-factor tokens); no intentional sharing is permitted.
5. You shall keep your registered information correct and up to date.
6. You shall promptly report known or suspected security breaches, credential compromise, or misuse to the security contact stated below; and report any compromised credentials to the relevant issuing authorities.
7. Reliance on the Services shall only be to the extent specified by any applicable service level agreements listed below. Use without such agreements is at your own risk.
8. Your personal data will be processed in accordance with the privacy statements referenced below.
9. Your use of the Services may be restricted or suspended, for administrative, operational, or security reasons, without prior notice and without compensation.
10. If you violate these rules, you may be liable for the consequences, which may include your account being suspended and a report being made to your home organisation or to law enforcement.
11. You shall provide appropriate acknowledgement of support or citation for your use of the resources/services provided as required by the body or bodies granting you access 

The administrative contact for this AUP is: support@hifis.net

The security contact for this AUP is: support@hifis.net

The privacy statements (e.g. Privacy Notices) are located at: Connected Services page


Version: 2.0

Date: 15.12.2023
