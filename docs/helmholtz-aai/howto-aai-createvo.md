# As a group manager: How to create and manage a group (VO)

## Check if you need a VO

You may **need a so-called "virtual organisation" (VO)**, if you

- want to manage multiple users, in hierarchical groups, for multiple services, and/or
- want to include users outside of Helmholtz.

This comes with a bit of overhead. Simple shares via links etc. may be more handy, depending on your use case.

## Request your VO

1. **We need the following basic infos** to create your new VO. Send this to [support@hifis.net](mailto:support@hifis.net?subject=[VO request]):
    - **A name** of the VO/group (replace whitespaces with underscores)
    - **A very brief description** of purpose (one sentence)
    - **At least one responsible person** from Helmholtz who is to take care of the overall group and its members
        - Note: that management rights can also be further delegated or distributed within the group, where needed
    - _Optional_: An acceptable use policy, binding joining users to a specific purpose. Templates here: [odt](policies/helmholtz-aai/05_VO-PP-Template.odt) / [pdf](policies/helmholtz-aai/05_VO-PP-Template.pdf).
    - More detailed information on the process is [here](./howto-vos.md).
1. You need **2nd factor authentication (2FA) for group management**:
    - If your center already asks for a 2nd factor authentication (2FA) during login:  
      Perfect, [you can skip](https://hifis.net/news/2025/01/09/Helmholtz-ID-simplifies-secure-login.html) this step.
    - Otherwise, you need to explicitly set 2FA for group management:
        - Log in at <https://login.helmholtz.de/home/>
        - Tab "Sign-In".
        - _Do not click on "Enable two-step authentication if possible"_, as it is unneeded for this purpose.
        - Go to "One time password".
        - Scan the QR code with an appropriate 2FA app, and confirm.

## Managing your new VO

4. **Once the group/VO is created, we will contact you.**
1. **Ensure proper authorization / access** for your group at the service(s) of interest:
    - Most likely, you (or any other group member) need to (re-) log in once to the service, in order to register the new VO/group there.
    - Contact the service owner (default: <support@hifis.net>) to grant access to the new group.
1. **Invite people**:
    - Log in to the group management endpoint at <https://login.helmholtz.de/upman/>, with 2FA.
    - Invite your users via Tab "Invitations" and adding their emails. Wait for invitations to be sent (progress bar on top of screen).
    - _Optionally_ manage subgroups and delegate responsibilities, [as described here](./howto-vo-management.md).
1. **Instruct your users** by pointing to [the corresponding steps on how to join a group/VO](howto-aai-joinvo.md).
