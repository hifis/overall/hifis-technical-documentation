# Service Onboarding

By connecting your local service to Helmholtz Cloud you can bring your service to more than 46.000 people at Helmholtz and beyond.

On this page you'll find the information about adding your service to the Helmholtz Cloud. Iniating your service application only takes a few minutes. After that HIFIS Cloud staff will reach out to you and guide you through the the onboarding process step by step. You can expect to complete the service onboarding process within a few weeks.

If you are stuck at any point or have questions, reach out to us at <support@hifis.net>.

Generally speaking the onboarding process includes these steps:

- **Initiate your service application**: The first step is initiating your service application using [Plony](https://plony.helmholtz.cloud). Click the button below, sign in with your Helmholtz ID and complete the forms. After the collection of required service information, the service integration team is going to assist you with the next steps.   
[Initiate your service application](https://plony.helmholtz.cloud/sc/service-application){ .md-button .md-button--primary } <br /><br />
- **Integrate Helmholtz ID login**: If your service allows users to sign in, you should integrate login via Helmholtz ID. If you already know how to set up OIDC login for your service, great! If not, service integration staff is going to provide advice. <br />
- **Set up automated resource management**: If your service offers resources on-demand, Helmholtz Cloud can help you automate user's resource management. We provide tools for automating processes that would normally be handled in custom grant application forms, back-and-forth in emails or ticketing systems. This lowers the administrative burden on your end. Check out the guide below on how to get started:

    [Integrate Resource Management](../resource-management/how-to-integrate-resource-management/){ .md-button .md-button--primary } <br /><br />

The onboarding process is described in detail in the [Process Framework For the Cloud Service Portfolio](../../process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.2_Onboarding-Process-for-new-Services.md).
