This section contains the technical reference for defining resource schemas. Before you embark on defining these schemas, we recommend you know exactly which data you need for your resource management tasks in your Helmholtz Cloud Agent and familiarize yourself with available Helmholtz ID attributes to filter on for policies.
Once done, you can upload these schemas to Plony from where they are then synced to Helmholtz Cloud.

If you're stuck at any point, reach out to us at [support@hifis.net](mailto:support@hifis.net) or [join our community chat](https://mattermost.hzdr.de/signup_user_complete/?id=tetkhy5b7frt8djp5ndosd8uqr&md=link&sbr=su).

## ResourceType

The `ResourceType` schema is the recommended starting point when defining schemas. It serves two main purposes:

1. It defines the data structure and format of the payload you expect to receive in your Helmholtz Cloud Agent.
2. It defines the layout and validation of the form presented to users when managing the resource on Helmholtz Cloud.

<figure markdown="span">
  ![A screenshot showing the resource-specific section of the resource form on Helmholtz Cloud.](../../../../images/cloud/resource_request_form_example.png){ width="60%" }
  <figcaption>The ResourceType describes the structure and validation of your resource form on Helmholtz Cloud and your Helmholtz Cloud Agent payload.</figcaption>
</figure>

### Attributes

#### `id`
A random UUID4 for your schema. When making changes to the schema, please do not change it when updating your schema. You can generate a new UUID4 with `python -c 'from uuid import uuid4; print(uuid4());'`.

#### `name`
A display name for your `ResourceType` this is going to be displayed on the resource's profile on Helmholtz Cloud.

#### `description`
A description for your `ResourceType` this is going to be displayed on the resource's profile on Helmholtz Cloud.

#### `json_schema`

The `json_schema` attribute contains a [JSON Schema](https://json-schema.org/) definition of your payload. Use the schema to catch errors as early as possible in the process to prevent users from making invalid requests. You may define properties at the first level (no nesting) with the types `string`, `integer` and `boolean` as well as associated format validations. Check the [JSON Schema](https://json-schema.org/) documentation for more information. Helmholtz Cloud supports the `2020-12 draft` version of the specification. `$ref`, `$recursiveRef` and `$dynamicRef` are not supported. A `property`'s description serves as the form field's description. The field's label is defined in the `ui_schema`. You may define default values for pre-filling form fields. If you define default values in a `Policy`, they take precedence over the values defined by the `ResourceType`.

#### `ui_schema`

!!! info "More data types and form controls"
    If you require special layout options, data types or form controls (e.g. sliders), please get in touch with us.

The `ui_schema` attribute contains a [JSONForms](https://jsonforms.io/) layout based on your `json_schema`. Here, you can set the field's order, set labels and reference the schema's `properties`. At this time only `VerticalLayout` and `Control` are supported.

### Annotated Example

The example below describes a team resource on the chat service Mattermost. Click the icons in the code to reveal the annotations.

```json title="ResourceType.json"
{
  "id": "bcfff2de-ed0b-435d-b650-02014e358b7b",
  "name": "Mattermost Team",
  "description": "A team is a digital workspace where you and your teammates can collaborate in Mattermost.",
  "json_schema": {
      "$schema": "https://json-schema.org/draft/2020-12/schema",
      "title": "MmTeamResourceSpecV1", // (1)!
      "description": "Specification of a Mattermost Team Version 1", // (2)!
      "type": "object",
      "properties": { // (3)!
          "team_name": {
              "description": "The desired team name of the new Mattermost team (0-9, a-Z, -_ ()[]).",
              "type": "string",
              "minLength": 2,
              "maxLength": 64,
              "pattern": "^[a-zA-Z][a-zA-Z0-9-_ /\\(\\)\\[\\]]+$" // (4)!
          },
          "team_slug": {
              "description": "The desired team URL of the new Mattermost team (a-z, 0-9, -).",
              "type": "string",
              "minLength": 2,
              "maxLength": 64,
              "pattern": "^[a-z][a-z0-9-]+$"
          },
          "invite_only": {
              "description": "The visibility of the new Mattermost team.",
              "type": "boolean",
              "default": false
          }
      },
      "required": ["team_name", "team_slug", "invite_only"] // (5)!
  },
  "ui_schema": {
      "type": "VerticalLayout",
      "elements": [
          {
              "type": "Control",
              "scope": "#/properties/team_name", // (6)!
              "label": "Team Name"
          },
          {
              "type": "Control",
              "scope": "#/properties/team_slug",
              "label": "Team Slug"
          },
          {
              "type": "Control",
              "scope": "#/properties/invite_only",
              "label": "Invite-only"
          }
      ]
  }
}
```

1. This is passed to your Helmholtz Cloud Agent when making resource requests.
2. This is not used at this time, set it to something descriptive.
3. Be sure to only define properties at the to level (no nesting).
4. This is also validated before the user can submit their request. This way you can reduce invalid requests.
5. Be sure to list all required properties here!
6. Note how this references the property path from `json_schema`.

Based on this, a payload arriving at your Helmholtz Cloud Agent could look like this:

```json title="CloudAgent.json"
{
    "type": "MmTeamResourceSpecV1",
    "target_entity": {
        "group_urn_target": null, // (1)!
        "user_id_target": "034cf70b-f2e6-4899-9eb3-8c6fa0d0cc90" // (2)!
    },
    "specification": { // (3)!
        "team_name": "Test Team",
        "team_slug": "test-team",
        "invite_only": true
    }
}
```

1. If this was a group resource, this would contain the group's URN.
2. This is a personal resource. If this was a group resource, this would be `null`.
3. This structure is specified by you in the `ResourceType`.

## Quota

A `Quota` allows you to limit the amount of resources allocated. Every resource requested on Helmholtz Cloud is linked to a `Quota`. Whenever a resource is requested, Helmholtz Cloud checks whether the limits imposed by the `Quota` would be exceeded by the request. A `Quota` allows you to restrict the overall sum of `integer` `properties` of resources allocated to that `Quota`. Note *this is not a per-resource limit*, but a limit on all resources linked to that `Quota`. If you want to introduce per-resource limits, use the `ResourceType` for per-request limits and `Policy` for limits for certain groups of users.

### Attributes

#### `id`
A random UUID4 for your schema. You can generate a new UUID4 with `python -c 'from uuid import uuid4; print(uuid4());'`.

#### `service_id`
The ID of your service in the catalog, You can find it on Plony or in the URL when opening your service's profile on Helmholtz Cloud.

#### `resource_type_id`
The ID of the `ResourceType`.

#### `name`
The `Quota`'s display name. It is shown to users if they have multiple `Policies` (linking to `Quota`s) to choose from.

#### `quota`
List of overall limits on `integer` `properties`. Set `property` to the name and `total` to the maximum limit you want to permit. You may define an empty list to create a quota with no limits. You may only reference a `property` once in this list and supply positive values. If you limit a value using a quota the `ResourceType`'s `json_schema` *must* define a `minimum > 0` or an `exclusiveMinimum >= 0` validation to prevent users from submitting negative values and thus breaking the `Quota`.

### Annotated Example
The example below describes a quota on a virtual machine resource. Click the icons in the code to reveal the annotations.

```json title="Quota.json"
{
    "id": "e2df7b90-6459-4740-bb50-7296895d3ddf",
    "service_id": "46f97142-9a84-4fc2-ba40-a8d8dc468a12",
    "resource_type_id": "94e1df23-77a1-4909-922e-56cbb3e1cf4b",
    "name": "Quota for Helmholtz Scientists",
    "quota": [
        {
            "property": "ram", // (1)!
            "total": 6400 // (2)!
        },
        {
            "property": "storage",
            "total": 12800
        }
    ]
}
```

1. Sum the overall amount of the property `ram` allocated to this `Quota`.
2. Set the overall limit of that property to 6400.

## Policy

Use the `Policy` to control who can manage resources for whom on a `Quota`. Every `Policy` is linked to a single `Quota`. Multiple `Policies` may link to the same `Quota`. You specify who can act using that policy based on a user's Helmholtz ID attributes in the `actor_requirements` attribute. Resources can belong to either individuals (personal resources) or groups. In addition, a policy can further restrict the payload's contents using the `json_schema` attribute. The properties on that [JSON Schema](https://json-schema.org/) must be a subset of the `ResourceType` of the linked `Quota`. When managing resources, a request must pass *both* the `ResourceType`'s and the `Policy`'s JSON Schema. Given the `Policy`'s central role for access control, take care when designing its restrictions and interactions with the other schemas.

### Attributes

#### `id`
A random UUID4 for your schema. You can generate a new UUID4 with `python -c 'from uuid import uuid4; print(uuid4());'`.

#### `name`
The `Policy`'s display name. It is shown to users if they have multiple `Policies` to choose from.

#### `quota_id`
The `Quota`'s ID this `Policy` grants access to.

#### `actor_requirements`
The requirements a user's Helmholtz ID profile must fulfill in order to use this `Policy`. At this time the following user attributes are supported:

- `eduPersonEntitlement`
- `eduPersonScopedAffiliation`
- `eduPersonAssurance`

Please refer to the Helmholtz ID documentation for more information on profile attributes.

The `actor_requirements` field's semantics are as follows:

- You must specify all fields supported by `actor_requirements` in your `Policy` (listed above)
- The profile attributes are represented as lists of `strings`:
    - `["value"]` means you're checking for the presence of `value1` in the attribute list, but the actor's list may include additional ones
    - `["value1", "value2"]` means both `value1` and `value2` must be present in the actor's attribute's list, but they may have additional ones in the list
    - `[]` means the actor must have the field set with any list contents, but you ignore its contents
    - `null` / `undefined` means you require the actor to not have the field or have it set to `null`

#### `target_entity`
This field describes which entities are valid owners of the resource managed using this `Policy`. Users choose the resource's owner based on `Policies` available to them in the first step of the resource request process. Possible values are:

- Set it to `self` for personal resources.
- Set it to a specific group's URN like `urn:geant:helmholtz.de:group:research-group` for allowing requesting for that group.
- Use a URN wildcard ending with `:` like `urn:geant:helmholtz.de:group:research-group:` for allowing requesting for all subgroups below that group e.g. `urn:geant:helmholtz.de:group:research-group:subgroup:project-a`.

You can find a list of groups registered at Helmholtz ID [here](../../../../../helmholtz-aai/list-of-vos/).

#### `json_schema`
The `json_schema` attribute contains a second [JSON Schema](https://json-schema.org/) used in addition to the `ResourceType`'s `json_schema` for validating a resource management action performed using the `Policy`. The resource must pass both the `ResourceType`'s and the `Policy`'s `json_schema`. This way, you can specify additional restrictions using `Policies`. If you do not want to add any additional restrictions, leave the list of `properties` empty. The `properties` listed in the `Policy` must be a subset of the `ResourceType`'s `properties`. A property's data type must be the same in both `json_schema`s. The same restrictions that apply to `ResourceTypes` (e.g. nesting, available data types...) apply for `Policies` as well. Refer to the `ResourceType`'s documentation for specifics.

#### `time_seconds`
Not used at this time. We're going to notify you if this changes. For now, you can set this to any positive integer value.

### Annotated Example
The example below describes a policy for providing access to managing personal team resource on the chat service Mattermost. Click the icons in the code to reveal the annotations.

```json title="Policy.json"
{
  "id": "a47fc9da-0a66-40cc-a643-bd9ddb3349a4",
  "name": "Mattermost Teams as Personal Resource", // (1)!
  "quota_id": "43762c80-aba4-4e2e-bd95-2107f1319240",
  "actor_requirements": {
      // Note this also specifies the authority at the end: #login.helmholtz.de
      "eduPersonEntitlement": ["urn:geant:helmholtz.de:group:Helmholtz-member#login.helmholtz.de"],
      "eduPersonScopedAffiliation": [],
      "eduPersonAssurance": ["https://refeds.org/assurance/ATP/ePA-1m"]
  },
  "target_entity": "self", // (2)!
  "json_schema": {
      "$schema": "https://json-schema.org/draft/2020-12/schema",
      "title": "MattermostPersonalTeamPolicy",
      "description": "Additional validation for this policy",
      "type": "object",
      "properties": {
        "team_name": {
              "description": "Your team's name must start with personal", // (3)!
              "type": "string",
              "minLength": 2,
              "maxLength": 64,
              "pattern": "^personal[a-zA-Z][a-zA-Z0-9-_ /\\(\\)\\[\\]]+$" // (4)!
        },
      }
  },
  "time_seconds": 9999 // (5)!
}
```

1. This is displayed to users if they have multiple `Policies` to choose from.
2. This is a `Policy` for personal resources.
3. This validation message gets displayed to the user in the resource form on Helmholtz Cloud.
4. Resources managed using this policy must also have their team name start with `personal` in addition to the format set by the `ResourceType`.
5. This is not used at this time and can be set to any positive integer value.

