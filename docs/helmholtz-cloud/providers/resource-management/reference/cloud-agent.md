This section contains the technical reference for the Helmholtz Cloud Agent. It explains the types of messages that are used to communicate between the Helmholtz Cloud and the Helmholtz Cloud Agent.

## Helmholtz Cloud Agent Messages

### ResourceAllocateV1

The `ResourceAllocateV1` message is sent by Helmholtz Cloud when the user requested a resource.

#### Attributes

##### `type`

The type of resource requested. Helmholtz Cloud takes the `name` from the `ResourceType`'s `title` attribute that you configured in the Service Catalog.

##### `target_entity`

The owner of the requested resource. It contains two fields:

- `user_id_target`: The `voPersonID` of the requesting user, if the `target_entity` in the policy is set to `self`. If the requested resource is a group resource this is set to `null`.
- `group_urn_target`: The group URN, if the resource is being requested for a group. It is `null` for personal resources.

##### `specification`

The specification of the resource. It depends on the `properties` as defined in the `json_schema` in the `ResourceType`.

#### Example

```json
{
    "type": "GroupStorageResourceSpecV1",
    "target_entity": {
        "group_urn_target": null,
        "user_id_target": "bd2dc138-e63a-46d4-a042-3c6b7d313b8f"
    },
    "specification": {
        "desired_name": "MySpecialFolderName",
        "quota": {
            "unit": "GB",
            "value": 100
        }
    }
}
```

### ResourceCreatedV1

The `ResourceCreatedV1` message is sent by the Helmholtz Cloud Agent when a resource was provisioned successfully.

#### Attributes

##### `id`

The internal ID of the resource at your service. This ID has to uniquely identify the provisioned resource on your end. It will be used for future messages referring to this resource e.g. when requesting deprovisioning.

#### Example

```json
{
    "id": "80bfa8ae-302b-40f6-bdb9-3c8cdbb651ec"
}
```

### ErrorV1

The `ErrorV1` message is sent by the Helmholtz Cloud Agent when a resource could not be provisioned.

#### Attributes

##### `type`

The class name of the exception that was raised in the resource allocate handler.

##### `message`

The message as defined in the raised exception. The message will be displayed to the requesting user in their resource list on Helmholtz Cloud.

#### Example

```json
{
    "type": "GroupStorageAlreadyExistsError",
    "message": "Group storage already exists"
}
```