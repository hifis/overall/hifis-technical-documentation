!!! info "User Documentation"
    This topic guide is written for service providers.

    If you're looking for information on how to request resources as a user, head [over here](../../users/getting-started.md#requesting-resources).

# How to Integrate Resource Management

Sometimes users need resources that require permissions beyond signing in with Helmholtz ID. For example, creating a new team in your chat service may need administrative privileges, or you collect additional information every time someone creates a survey in the tool offered by you. Maybe you already have manual processes in place to manage resources like this. However, this puts a burden on your operations staff and can be error-prone while also taking longer to complete than an automated solution. Helmholtz Cloud provides you with the tools needed to build automated resource lifecycle management into your services. Once you set up automation, users get self-service access to your service's resources while streamlining processes on your end.

The following topic guide outlines how the parts that make this possible fit together. This guide covers technical perspective of resource management. Please refer to our [getting started guide](../../getting-started/) and your service integration contact for administrative and policy matters.

## Basic Resource Requests

<figure markdown="span">
  ![A diagram showing how a user requests a chat resource on Helmholtz Cloud and how Helmholtz Cloud forwards requests to be processed by the Helmholtz Cloud Agent.](../../../images/cloud/resource_request_mattermost.png)
  <figcaption>A simplified diagram of requesting a chat service's resource on Helmholtz Cloud.</figcaption>
</figure>

The diagram above outlines requesting a chat service's resource on Helmholtz Cloud:

0. The user picks a resource to provision. Depending on their permissions they're guided through a multistep form where they provide the details of their resource request.
0. Helmholtz Cloud checks the request for validity (i.e. structure, permissions, quotas...) and forwards it to the provider's Helmholtz Cloud Agent.
0. The provider's Helmholtz Cloud Agent performs the necessary steps to provision the resource (e.g. API calls to the service).
0. Once provisioning is finished, the Helmholtz Cloud Agent notifies Helmholtz Cloud that the resource is ready to use or that provisioning has failed.
0. The result of the provisioning process shows up on the user's dashboard on Helmholtz Cloud.

Users have a single self-service point of entry at Helmholtz Cloud for requesting resources. You as the provider can operate the Helmholtz Cloud Agent on-premises and perform arbitrary actions to provision resources. This is achieved by code implemented by you. Actions can range from fully-automated API calls to opening tickets with information from the form at Helmholtz Cloud on internal queues. As long as the Helmholtz Cloud Agent sends and receives messages in the expected format you have full control and flexibility over how you organize the provisioning process. This is made possible by the customizable structure of the resource request process and described in more detail in the sections below.

!!! info "Support for editing and deprovisioning resources"
    Keen readers may have noticed this section focuses on creating / provisioning resources only. Helmholtz Cloud is evolving continuously. As the first part of the resource management feature-set support for provisioning resources has been implemented. Support for editing and deprovisioning resources through Helmholtz Cloud is not available yet. This is going to work much like the provisioning implementation in your Helmholtz Cloud Agent. Now is a great time to get started with integrating your service as your feedback shapes the implementation of features to come. We're going to notify resource providers once support is ready.

## Automating your Service

Integrating service automation may seem daunting at first, but if you break it down, things should start falling into place. Once a user places a request, Helmholtz Cloud forwards it to your Helmholtz Cloud Agent (HCA) which you can run on-premises at your center. We provide you with a Python library for implementing resource management automation, so anything you can do in Python (calling APIs, sending email, calling other programs...) can be done as part of the request. Of course, you're free to implement a fully custom Helmholtz Cloud Agent in a language of your choice for maximum flexibility. Based on our experience most time is spent on getting the technical integration of the Helmholtz Cloud Agent right. Therefore, we provide project templates and guides like this to get you started quickly. Coming up with a rough plan before you start the implementation process should also help you to avoid dead ends. We propose you break technical integration down like this:

0. **Survey existing processes and technical interfaces to your service and make a plan**: If you already run the service internally, survey how resources are currently managed. Is there automation in place already? Does the service provide an API you can use to provision, modify and deprovision resources? If there is no API, can you write scripts or open tickets in an automated manner? Talk to the people managing the service, if applicable. Maybe you can help improve internal processes. Remember Helmholtz Cloud can filter requests based on form field contents, user attributes and track quotas on form fields for you. Maybe you can start provisioning resources on this service through Helmholtz Cloud even for internal users. Make a plan for your implementation and collect all the documentation you need for approaching the task.
0. **Prototype your Helmholtz Cloud Agent implementation locally**: Next, it's time to roll up your sleeves and start coding. If you choose to go with our Python library we have prepared a project template and guide for you [here](../how-to-build-your-own-hca/) to get you going in a few moments. This can be done completely locally. Prototype the automation and by the end of this you should have a good idea of what data you need from users to process resource requests.
0. **Get your service into the service catalog**: With the data schema in hand you should be able to assemble the schemas needed for defining your resource's data structure, form layout, quotas and policies.  Upload them to Plony to get them into the service catalog. This will enable Helmholtz Cloud to show your resource to users. You can read more on this in the section below.
0. **Test your integration on Helmholtz Cloud**: Once your data schemas are ready, we recommend you to perform a limited test run of your implementation by setting-up a restrictive policy which only allows a predefined group of test users to provision your resources. You're going to receive production credentials to connect your Helmholtz Cloud Agent to Helmholtz Cloud and you can get started testing and fixing any remaining issues before general launch.
0. **Launch your integration for your users**: Once you are satisfied with the results, you can open up the policy for a more general audience and go into production.

## Resource Schemas

Once you have a general idea of what data you need from users to manage resources, you can move ahead with defining formal schemas for Helmholtz Cloud. The schemas you're going to define are named `ResourceType`, `Policy` and `Quota`. Each of these allows you to customize a different aspect of your integration:

- `ResourceType`: The resource form and data format
- `Policy`: Who is allowed to manage which resources on whose behalf
- `Quota`: Quota limits for resources

The schemas are written in JSON and based largely on open standards with rich ecosystems such as [JSON Schema](https://json-schema.org/) and [JSONForms](https://jsonforms.io/). To see how this all fits together, let's look at how Helmholtz Cloud processes a typical resource request:

<figure markdown="span">
  ![A diagram showing the checks a resource request made by a user passes before it is forwarded to and processed by the Helmholtz Cloud Agent.](../../../images/cloud/resource_request_process.png){ width="100%" }
  <figcaption>Behind the scenes a resource request passes multiple checks before it is forwarded to your Helmholtz Cloud Agent.</figcaption>
</figure>

1. **User places request**: The user chooses an on-demand resource to request from the catalog. They select who the resource is for (personal or for a group) and pick a policy/quota if multiple are available to them. Then, they complete the resource form and submit the request. The user interface hides options unavailable to them to prevent invalid requests.
2. **`ResourceType` Validation**: Helmholtz Cloud checks whether the data submitted by the user matches the format specified by your `ResourceType`. This validates the basic structure of the request data sent in the form.
3. **`Policy` Validation**: Next, Helmholtz Cloud checks whether the user is allowed to make the request for their intended target. In other words it checks whether the user's Helmholtz ID attributes match the requirements you defined in the `Policy`. Additionally, it validates the form input against the schema you set in the `Policy` (if any). The `Policy`'s schema is a *subset* of the `ResourceType`'s schema. This allows you to impose additional restrictions on requests made using a specific `Policy`. For example, you may enforce a certain naming scheme for a group of users.
4. **`Quota` Validation**: If the `Policy` checks passed, Helmholtz Cloud checks whether the request would exceed the `Quota` defined by you.
5. **Forwarding to the Helmholtz Cloud Agent**: Finally, Helmholtz Cloud forwards the request to your Helmholtz Cloud Agent.
6. **Processing by the Helmholtz Cloud Agent**: The custom code for managing the resource at your service is run.
7. **Response to Helmholtz Cloud**: Your Helmholtz Cloud Agent passes the result back to Helmholtz Cloud.

Once this process is complete, the user is going to see the result on their resource page on Helmholtz Cloud. Read [the service catalog reference](../reference/resource-schemas/) to learn how `ResourceType`, `Policy` and `Quota` work in detail.

