# What Makes a Service Valuable for the Helmholtz Cloud?

## Indicators
These indicators demonstrate that your service would be a valuable contribution to the Helmholtz Cloud:

* Your user base can extend from your own Helmholtz Center to at least one other center and/or research area.
    * Services that meet this requirement can support research at other centers, adding value to the services.
* The popularity of your service increases by presenting it in public (in the Helmholtz Cloud Portal).
* The Helmholtz Cloud Portal can showcase an interesting selection of services to specific research communities (e.g., data hub services for the research field Earth & Environment), which may have been unknown to users before (using keywords/tags).
* Unified access (for services with login) for all Helmholtz Centers (and beyond) is provided through Helmholtz ID.

## Not Required
HIFIS does not specify requirements for:

* The content of the service (what is actually offered) and service levels (e.g., availability, limitations, number of users, etc.).
* Whether the service is open to all Helmholtz Centers or only to dedicated centers or user groups.
* The minimum duration for which a service must be provided (the service provider is only obliged to adhere to the minimum provision time indicated during service onboarding).
* Obligations (if and to what extent) the using center needs to fulfill (e.g., licenses, hardware) to use the service.

## Exclusion Criteria
Due to legal and organizational reasons, a service must fulfill specific exclusion criteria to become a Helmholtz Cloud service. These exclusion criteria are defined in the Process Framework and integrated into the [Application Form in Plony](https://plony.helmholtz.cloud/sc/service-application) (login required).
   
The exclusion criteria are the "hard facts" – services that cannot meet this basis cannot become a Helmholtz Cloud service. There is no further evaluation beyond checking the exclusion criteria. You can find all criteria from the Application Form in the [Process Framework, Chapter Onboarding process for new services](https://hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.2_Onboarding-Process-for-new-Services/#application-for-becoming-a-helmholtz-cloud-service).
  
Besides exclusion criteria, there are no further requirements a service needs to fulfill – it is up to the service provider to define the conditions for service usage (e.g., limitation of users, availability, support times).