# Updating Service Information
## The Change Request
As a Service Provider, you can change the service information at any time. Therefore, please login to [Plony](https://plony.helmholtz.cloud/), select your service and use the tab `Change Request`.

## The Annual Service Portfolio Review Process

A Service Portfolio needs to be maintained to continuously meet the user’s requirements for services and the criteria defined from HIFIS side. Therefore, the primary goal of the Service Portfolio Review process is to check whether the services in and the processes around the Service Portfolio continue to fulfill defined requirements.

Each Service Providers will recieve a notification when the Service Review Process starts. As a service owner or service manager, you can check in Plony the actuality of the service information. If necessary, you can update the information in each fields. The annual review is scheduled to begin in the fall.

You find the detailed description of the [Service Portfolio Review Process](../../process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.6_Review-Process-for-the-Service-Portfolio.md) as a part of the Process Framework for Helmholtz Cloud Service Portfolio.
