# Users: Getting Started

Here, we guide you through the first steps of using Helmholtz Cloud. We’ll set up your Helmholtz ID and browse [Helmholtz Cloud](https://helmholtz.cloud) for services. You should be able to complete these steps in just a few minutes. Even if this guide may look slow-paced, we encourage you to read these steps carefully to save you some confusion later on.

If you are looking for a more general overview over what Helmholtz Cloud has to offer, [head over here](../../cloud-services/README.md). If you’re stuck at some point, feel free to email us at <support@hifis.net>. We’ll get back to you as soon as possible to resolve your problem.

## Prerequisites

!!! info "Conditions Apply"
    Helmholtz Cloud resources are subject to their [terms of use](https://helmholtz.cloud/terms-of-use). For example, it is not permitted to use the Helmholtz Cloud resources for private or economic purposes as defined by EU state aid law (EU-Beihilferecht). You can review the terms of use and examples of what is allowed and what not [here](https://helmholtz.cloud/terms-of-use). Contact your center's legal department for individual advice on these matters.

You will use Helmholtz ID to sign in to Helmholtz Cloud. Helmholtz ID is your central identity for accessing Helmholtz Cloud, a bit like your ‘Cloud ID Card’. It holds your profile information and access permissions. Best of all, you do not have to remember yet another password: all you need to get started are your access credentials at your home institution. Helmholtz ID supports signing in through (in order of preference):

1. [Helmholtz Centers](../../helmholtz-aai/list-of-connected-organisations.md#helmholtz-centres)
2. [Organizations Connected to eduGAIN](../../helmholtz-aai/list-of-connected-organisations.md#edugain), this includes many universities and research organizations around the world
3. [Social Identity Providers](../../helmholtz-aai/list-of-connected-organisations.md#social-identity-providers) like ORCID, GitHub and Google

Depending on the option you choose you’ll get different levels of access to Helmholtz Cloud. One thing to keep in mind is that generally signing in via Helmholtz center is going to give you the widest access to resources by default. Also, when you’re signing in through your Helmholtz center one day and then e.g. through GitHub the next, you’re going to end up with two separate Helmholtz ID identities. It is best to stick with one from the beginning, ideally your local center’s account. Collaboration partners without an account at a Helmholtz center should prefer signing in via eduGAIN, if possible. Social identity providers can be used as a fallback if neither of these two options are available to an individual.

## Creating your Helmholtz ID

<figure markdown="span">
  ![Helmholtz ID](../../images/cloud/helmholtz_id.svg){ width="60%" }
  <figcaption>You can think of Helmholtz ID as your ID card for Helmholtz Cloud</figcaption>
</figure>

First, we’re going to create your Helmholtz ID. If you already have a Helmholtz ID account or if you only want to browse services without signing in, you can skip this step.

1. Open [login.helmholtz.de](https://login.helmholtz.de/home/).
2. Pick the login provider you chose in the [Prerequisites](#prerequisites).
3. Sign in with your login provider and follow the instructions on screen to create your account. More detailed instructions are [available here](https://www.hifis.net/tutorial/2021/06/23/how-to-helmholtz-aai).

You’re now ready to sign in to Helmholtz Cloud services! By signing in to [login.helmholtz.de](https://login.helmholtz.de/home/) you can review your technical profile information and trusted applications (such as Helmholtz Cloud Services) anytime.

## Browsing Services

You can browse all available Helmholtz Cloud services at [helmholtz.cloud](https://helmholtz.cloud):

1. Visit [helmholtz.cloud](https://helmholtz.cloud).
2. Click on ['Browse Services'](https://helmholtz.cloud/services/).
3. Filter services according to your interests and click on a service’s card to open its profile and reveal more information about it.

[helmholtz.cloud](https://helmholtz.cloud), sometimes also referred to as the Helmholtz Cloud Portal, is your central point of access to Helmholtz Cloud services: from discovering services and requesting resources all the way to you requesting the deletion of your data.

## Accessing Services

Once you’ve found a service you want to use on [helmholtz.cloud](https://helmholtz.cloud/services/), study its profile. There, you’ll find general information on the service, its provider, where data is stored, where to get help, how to get access and any limitations.

Depending on the information listed in the profile’s ‘How to get Access’ section, there generally are three ways of accessing services:

- *Sign in with Helmholtz ID*: All or most of the service’s functionality is available by simply signing in to the service using your Helmholtz ID. Click the ‘Go to service’ button on its profile. On the service’s page you should find a login button for Helmholtz ID.

- *Requesting Resources Through Helmholtz Cloud*: In addition to connecting to Helmholtz ID, some services offer advanced resources. Sign in to [helmholtz.cloud](https://helmholtz.cloud) and click the ‘Request’ button next to the resource you need to start the process. More detailed instructions are available in the [Requesting Resources](getting-started.md#requesting-resources) section.

- *Manual Steps Required*: Even though we aim to make the experience of using Helmholtz Cloud services as seamless as possible through Helmholtz ID and resource requests on Helmholtz Cloud, not all services have reached this degree of automation yet. You’ll find the necessary steps to get access in the service profile’s ‘How to get Access’ section.

When you do not want to use a service anymore, sign in to Helmholtz Cloud and request deletion of your data from the profile page next to the support links.

### Requesting Resources

<figure markdown="span">
  ![Helmholtz Cloud Resource Requests](../../images/cloud/resource_request.svg){ width="80%" }
  <figcaption>Helmholtz Cloud is your central point of access to managing your digital resource requests</figcaption>
</figure>

Some services offer access to resources directly through Helmholtz Cloud.

- Sign in to [helmholtz.cloud](https://helmholtz.cloud).
- Click on ['Browse Services'](https://helmholtz.cloud/services/). Open the service profile.
- If the service offers additional resources on request you'll see a section like this in the profile:
<figure markdown="span">
  ![Helmholtz Cloud Resource Requests](../../images/cloud/request_service_profile.png){ width="80%" }
  <figcaption>Once you're logged-in the service's profile will show you resources available upon request.</figcaption>
</figure>
- Pick the resource you want to request and follow the instructions.

## Get Connected
That’s it! You’re all-set to get started with Helmholtz Cloud! If you’d like to dig a little deeper join one of our [community channels](https://mattermost.hzdr.de/hifis-community/channels/town-square) or check out [our guide](../../../helmholtz-aai/howto-aai-createvo/) on how to get your whole group onboarded.

## Getting Help

Feel free to email us at <support@hifis.net>. We’ll get back to you as soon as possible to resolve your problem.