# Create and manage user groups

You can manage your overarching working groups for Helmholtz Cloud using Helmholtz ID, using a representation called **Virtual Organisation (VO)**.
Here, you can define and organise hierarchies, roles and specific access rights to resources in service.
You can add and manage users from Helmholtz and outside, and delegate management permissions.

On top, VOs are a precondition to:

- let users from outside Helmholtz access shared resources on Helmholtz Cloud
- allow to request certain service resources for your group via the Helmholtz Cloud Portal

The group/VO management involves the following basic steps:

- **Check if you need a VO**:
Many use cases can be fulfilled without a VO.
However, if you plan to manage a medium-sized (or larger) group, over long time, and/or different roles with common usage of multiple services for all members, you probably need a VO.
- **Create a VO**:
Once you decided you need a VO, register it.
We need some basic information from you for this (title, contact, etc).
Currently this involves some manual mailing, we are working on improved automation of the process.  
[Register a new group / VO](../../../helmholtz-aai/howto-vos/){ .md-button .md-button--primary } <br />
- **Let users in**:  
[Invite users to your group / VO](../../../helmholtz-aai/howto-vo-management/#inviting-new-users){ .md-button .md-button--primary } <br />
  Note:
    - optionally manage subgroups and delegate responsibilities.
    - Wait for invitations to be sent.
    - Point your users to [the corresponding steps on how to join a group/VO](../../../helmholtz-aai/howto-aai-joinvo/) for basic How-to.
- **Define roles** for resource management (_to be done_):
Some cloud services allow the centralized management of resources via the Helmholtz Cloud Portal.
Service providers define this via service-specific [resource management schemas](../../../helmholtz-cloud/providers/resource-management/how-to-integrate-resource-management/#resource-schemas).  
_In the near future you as a group/VO manager can define resource management roles in your group / VO, stay tuned!_

If you are stuck at any point or have questions, reach out to us at <support@hifis.net>.
