# Documentation on services by HIFIS

This repository contains the documentation for services built by HIFIS.

The documentation is deployed to and publically available at
**<https://hifis.net/doc/>**.

## Conventions

Markdown is the common format for the documentation. For the general syntax see <https://www.markdownguide.org/basic-syntax>.

## Contribute

You can contribute by logging in via Helmholtz AAI, create a branch from master, do your suggested changes and invite a project maintainer to review (and possibly merge) your changes.

## Local builds, External Resources

This documentation repository uses submodules.
For local builds, clone this repository *recursively*, with:
```
git clone --recursive git@codebase.helmholtz.cloud:hifis/overall/hifis-technical-documentation.git
```

If you have cloned the repository already, you might need to update the submodules with
```
git submodule sync --recursive
git submodule update --remote --recursive
```

This documentation uses the following plugins.
For local builds, you need to install them as necessary:

* Kroki diagrams: See [introduction here](https://hifis.net/tutorial/2021/06/02/easy-diagram-creation-in-gitlab).
    `pip install mkdocs-kroki-plugin`.
* The [redirect plugin](https://pypi.org/project/mkdocs-redirects/)
  `pip install mkdocs-redirects`
* The [material theme](https://github.com/squidfunk/mkdocs-material)
  `pip install mkdocs-material`

The repository is public readable and it is writable for Helmholtz AAI logged in persons.

### Containers

This repository contains support for development containers (`.devcontainer`), including spellchecking and grammar checking with a local version of [LanguageTool](https://languagetool.org/de/dev) in Visual Studio Code as well as live previews of your edits in your browser. Choose this if you are comfortable with git and using development tools on your computer. While the experience on Linux should be fairly seamless, please be aware that this may require some tinkering on Windows and macOS.

#### Getting started with devcontainers
If you already know how to run devcontainers on your machine, you can skip this section. You can find a basic outline of the process below. However, as always, **apply your own thinking and double-check, especially around licensing and installing software on your computer**, as this can differ from organization to organization. Providing detailed instructions for all operating systems and environments is out of scope here, there’s a multitude of tutorials available on the Web.

1. [Visual Studio Code](https://code.visualstudio.com/) needs to be installed and configured to your liking
2. Install the [Dev Containers Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
3. You also need a way to run Docker Compose container setups on your computer. Most guides recommend the Docker Desktop app, however, it most probably requires a paid subscription. If you do not have a Docker subscription, Podman could be an alternative:
    - [Install Podman](https://podman.io/)
    - Install [podman-compose](https://github.com/containers/podman-compose?tab=readme-ov-file#installation). Windows users may need to install [Python](https://www.python.org/) first to get the `pip` command working.
    - Get the path of the `podman-compose` binary on the terminal with `which podman-compose` (`where podman-compose` on Windows)
      - Note down the *absolute* path e.g. turn `~/.local/bin/podman-compose` into `/home/YOURUSER/.local/bin/podman-compose`
    - Set the absolute path as the `Docker Compose Path` in the Visual Studio Code settings for the `Dev > Containers` extension, so it looks for `podman-compose` instead of Docker Compose

#### Using this project’s devcontainer
This project contains a compose setup with a local Python container for mkdocs and supporting containers e.g. for spellchecking.

- Clone this repository and its submodules as documented above
- Open the command palette (`ctrl+shift+p`) and select `Dev Containers: Reopen in Container`
- Wait for the process to finish
- Edit a markdown file, you should be getting spellchecking on saving the file
- To get live previews:
  - In Visual Studio Code click `Terminal > New Terminal` to open a shell in your devcontainer
  - Run `pipenv shell` to activate the `pipenv` virtual environment for the documentation project
  - Run `mkdocs serve` to start the mkdocs live server in the container
  - Click the `Ports` tab in Visual Studio Code, you should be seeing port 8000 opened. When you hover your mouse over the forwarded address, you should see a globe icon appear. Click it and the live preview of the docs will appear.
  - Sometimes the `mkdocs serve` process will get stuck (e.g. when you’re changing basic configuration). Restart the server in that case (`ctrl+c` to stop and `mkdocs serve` to start again)

## Automatic Builds (CI)

* Master build review app (password required):
  <https://hifis-review-app.hzdr.de/review-apps/hifis-overall-hifis-technical-documentation/master/>
* Download latest master build:
  <https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/jobs/artifacts/master/download?job=build>
* Browse latest master build:
  <https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/jobs/artifacts/master/browse/public?job=build>
